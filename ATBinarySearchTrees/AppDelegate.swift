//
//  AppDelegate.swift
//  ATBinarySearchTrees
//
//  Created by Dejan on 08/08/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        testBST()
        
        return true
    }
    
    private func testBST() {
        let rootNode = Node(key: 123, value: Car("Red"))
        let binaryTree = BinarySearchTree(rootNode)
        
        binaryTree[99] = Car("Blue")
        binaryTree[126] = Car("Green")
        binaryTree[44] = Car("Yellow")
        binaryTree[32] = Car("Mini")
        binaryTree[234] = Car("Big car")
        binaryTree[432] = Car("A bus")
        binaryTree[104] = Car("Jeep")
        binaryTree[100] = Car("Batmobile")
        binaryTree[101] = Car("Sedan")
        binaryTree[105] = Car("Firetruck")
        
        print("::::::::::Testing subscript::::::::::")
        print("Blue car: ", binaryTree[99]?.name)
        
        binaryTree[99] = Car("Not Blue")
        
        print("Blue car: ", binaryTree[99]?.name)
        
        binaryTree[99] = nil
        
        print("Blue car: ", binaryTree[99]?.name)
        
        print("::::::::::Testing min/max::::::::::")
        print("MIN car: ", binaryTree[binaryTree.min]?.name)
        print("MAX car: ", binaryTree[binaryTree.max]?.name)
        
        print("::::::::::Delete max::::::::::")
        binaryTree[432] = nil
        print("MAX car: ", binaryTree[binaryTree.max]?.name)
    }
}

class Car {
    
    // A simple example with one variable to demo the use of a class as a value.
    let name: String
    
    init(_ name: String) {
        self.name = name
    }
}

